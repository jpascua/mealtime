import React, { Component } from 'react'

import MealItem from './MealItem';

class MealsList extends Component {
  render() {
    return this.props.meals.map((meal, index) => {
      const uuidv1 = require('uuid/v1');
      console.log(meal);
      return (<MealItem key={ uuidv1() } index={index} meal={meal} meals={this.props.meals} onDeletePress={this.props.onDeletePress}/>);
    });
  }
}

export default MealsList;
