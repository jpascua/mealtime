import React, { Component } from 'react';
import { View, Text, StyleSheet, Button, TouchableHighlight, ScrollView, AsyncStorage } from 'react-native';
import Moment from 'moment';

import MealItem from './MealItem';
import MealsList from './MealsList';

class MealsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      meals: []
    }

    retrieveData = async () => {
      const value = await AsyncStorage.getItem('meals');
      if (value !== null) {
        this.setState({ meals: JSON.parse(value) });
      }
    };

    retrieveData();
  }

  totalCalories = () => {
    sum = 0;

    this.state.meals.map((meal) => {
      sum += meal.calories;
    })

    return sum;
  }

  render() {
    // Sort meal objects chronologically (descending order) before displaying
    this.state.meals.sort((a, b) =>  Moment(b.date).valueOf() - Moment(a.date).valueOf());

    return (
      <View style={styles.container}>
        <View style={styles.headerTextContainer}>
          <Text style={styles.headerNameText}>Name</Text>
          <Text style={styles.headerCaloriesText}>Calories</Text>
        </View>

        <View style={styles.listContainer}>
          <ScrollView>
            <MealsList meals={this.state.meals} onDeletePress={
              (index) => {
                this.state.meals.splice(index, 1);
                this.setState({ meals: this.state.meals });

                AsyncStorage.setItem('meals', JSON.stringify(this.state.meals));
              }
            } />
          </ScrollView>
        </View>

        <View style={styles.footerTextContainer}>
          <Text style={styles.footerTotalText}>Total</Text>
          <Text style={styles.footerSumText}>{this.totalCalories()}</Text>
        </View>

        <View style={styles.buttonContainer}>
          <TouchableHighlight
            style={styles.buttonHighlight}
            onPress={
              () => this.props.navigation.navigate('AddMeal',
              {
                callback: (meal) => {
                  this.setState({ meals: [...this.state.meals, meal] });

                  AsyncStorage.setItem('meals', JSON.stringify([...this.state.meals, meal]));
                }
              })
            }
          >
            <Text style={styles.buttonText}>Add Meal</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

export default MealsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerTextContainer: {
    flexDirection: 'row',
    padding: 15,
    backgroundColor: '#ccc'
  },
  headerNameText: {
    flex: 1,
    textAlign: 'left',
    fontSize: 20,
    fontWeight: 'bold'
  },
  headerCaloriesText: {
    flex: 1,
    textAlign: 'right',
    fontSize: 20,
    fontWeight: 'bold'
  },
  listContainer: {
    flex: 4
  },
  footerTextContainer: {
    flexDirection: 'row',
    padding: 15,
    backgroundColor: '#ccc'
  },
  footerTotalText: {
    flex: 1,
    textAlign: 'left',
    fontSize: 20,
    fontWeight: 'bold'
  },
  footerSumText: {
    flex: 1,
    textAlign: 'right',
    fontSize: 20,
    fontWeight: 'bold'
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
  },
  buttonHighlight: {
    width: 220,
    backgroundColor: '#2196F3',
  },
  buttonText: {
    textAlign: 'center',
    padding: 20,
    color: 'white'
  }
});
