import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet, Button, TouchableHighlight, Image } from 'react-native';
import { Input } from 'react-native-elements';
import DatePicker from 'react-native-datepicker';

class AddMealScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <Image style={styles.image} source={{ uri: this.state.photo }} />
        </View>
        <View style={styles.imageButtonContainer}>
          <TouchableHighlight
            style={styles.buttonHighlight}
            onPress={
              () => this.props.navigation.navigate('Camera',
              {
                callback: (photo) => {
                  this.setState({photo});
                }
              })
            }
          >
            <Text style={styles.buttonText}>Add Photo</Text>
          </TouchableHighlight>
        </View>
        <View>
          <DatePicker
            style={styles.datePicker}
            date={this.state.date}
            mode="datetime"
            placeholder="select date"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0
              },
              dateInput: {
                marginLeft: 36
              }
              // ... You can check the source to find the other keys.
            }}
            onDateChange={(date) => this.setState({date: date})}
          />
        </View>
        <View style={styles.formsContainer}>
          <Input
            label='Name'
            onChangeText={(name) => this.setState({name})}
            containerStyle={{padding: 10}}
          />
          <Input
            label='Location'
            onChangeText={(location) => this.setState({location})}
            containerStyle={{padding: 10}}
          />
          <Input
            label='Calories'
            onChangeText={(calories) => this.setState({calories})}
            containerStyle={{padding: 10}}
          />
        </View>

        <View style={styles.formsButtonContainer}>
          <TouchableHighlight
            style={styles.buttonHighlight}
            onPress={() => {
              // Call callback function from MealsScreen
              const callback = this.props.navigation.getParam('callback');
              callback({
                name: this.state.name,
                location: this.state.location,
                calories: parseInt(this.state.calories, 10),
                date: this.state.date,
                photo: this.state.photo
              });
              this.props.navigation.navigate('Meals');
            }}
          >
            <Text style={styles.buttonText}>Submit</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

export default AddMealScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 15
  },
  imageContainer: {
    alignItems: 'center',
  },
  image: {
    width: 120,
    height: 120,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#CCC'
  },
  imageButtonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15
  },
  datePicker: {
    width: 300
  },
  formsContainer: {
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  formsButtonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonHighlight: {
    width: 220,
    backgroundColor: '#2196F3'
  },
  buttonText: {
    textAlign: 'center',
    padding: 20,
    color: 'white'
  }
});
