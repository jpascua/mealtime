import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { Camera, Permissions } from 'expo';

class CameraExample extends React.Component {
  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
  };

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  snap = async () => {
    if (this.camera) {
      let photo = await this.camera.takePictureAsync();

      // Call callback function from AddMealScreen
      const callback = this.props.navigation.getParam('callback');
      callback(photo.uri);
      this.props.navigation.navigate('AddMeal');
    }
  };

  render() {
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    } else {
      return (
        <View style={styles.container}>
          <Camera ref={ref => { this.camera = ref; }} style={styles.camera} type={this.state.type} />
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.buttonHighlight}
              onPress={() => {
                this.snap();
              }}>
              <Text style={styles.buttonText}>Snap</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  }
}

export default CameraExample;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  camera: {
    flex: 5
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  buttonHighlight: {
    width: 220,
    backgroundColor: '#2196F3'
  },
  buttonText: {
    textAlign: 'center',
    padding: 20,
    color: 'white'
  }
});
