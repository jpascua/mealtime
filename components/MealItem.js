import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Swipeout from 'react-native-swipeout';
import Moment from 'moment';

class MealItem extends Component {
  swipeoutButtons = [
    {
      text: 'Delete',
      backgroundColor: '#FF2B2B',
      underlayColor: '#000',
      onPress: () => { this.deleteMeal() }
    }
  ]

  deleteMeal = () => {
    this.props.onDeletePress(this.props.index);
  }

  render() {
    const date = this.props.meal.date;

    return (
      <View style={this.props.index == this.props.meals.length - 1 ? styles.containerNoBorder : styles.containerBorder}>
        <Swipeout right={this.swipeoutButtons} backgroundColor='white' buttonWidth={120}>
          <View style={styles.container}>
            <View>
              <Image style={styles.image} source={{ uri: this.props.meal.photo }} />
            </View>
            <View style={styles.mealDetailsContainer}>
              <Text style={styles.mealTitleText}>{this.props.meal.name}</Text>
              <Text>{this.props.meal.location}</Text>
              <Text>{ date != undefined ? Moment(date).format('MM/DD/YY - hh:mm A') : date }</Text>
            </View>
            <View style={styles.mealCaloriesContainer}>
              <Text style={styles.mealCaloriesText}>{this.props.meal.calories}</Text>
            </View>
          </View>
        </Swipeout>
      </View>
    );
  }
}

export default MealItem;

const styles = StyleSheet.create({
  containerBorder: {
    borderBottomWidth: 1,
    borderColor: '#ccc'
  },
  containerNoBorder: {
    borderBottomWidth: 0,
    borderColor: '#ccc'
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    padding: 15
  },
  image: {
    width: 64,
    height: 64
  },
  mealDetailsContainer: {
    flex: 5,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15
  },
  mealTitleText: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  mealCaloriesContainer: {
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  mealCaloriesText: {
    fontSize: 20
  }
});
