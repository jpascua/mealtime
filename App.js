import React, { Component } from 'react';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import HomeScreen from './components/HomeScreen';
import MealsScreen from './components/MealsScreen';
import AddMealScreen from './components/AddMealScreen';
import CameraScreen from './components/CameraScreen';

class App extends Component {
  render() {
    return <TabNavigator />;
  }
}

export default App;

const MealsStack = createStackNavigator(
  {
    Meals: MealsScreen,
    AddMeal: AddMealScreen,
    Camera: CameraScreen
  }
);

const TabNavigator = createBottomTabNavigator(
  {
    Home: HomeScreen,
    Meals: MealsStack,
    Log: HomeScreen
  },
  {
    initialRouteName: 'Meals',
  }
);
